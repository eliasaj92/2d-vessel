function [d_ratio] = distanceFromVessel2DMesh2(Vessel, x, y, Radius_Array)

%This code calculates the distance of each pixel from the vessel
%This uses mesh grids for calculations of the minimum

[X,Y] = meshgrid(x,y);

c = size(Vessel,2);


d_ratio = inf*ones(size(X));
%loc = ones(size(X)); %stores index of point on vessel

for i = 1:c
    Rad = assignRadiusToPixel(i, Radius_Array);
    mx = Vessel(1,i);
    my = Vessel(2,i);
    tdist = sqrt((X-mx).*(X-mx) + (Y-my).*(Y-my));
    tdist_r = tdist/Rad;
    ind = tdist_r < d_ratio;
    d_ratio(ind) = tdist_r(ind);
    %loc(ind) = i;
end


end