-assignRadius: Gets radii for daughter branches based on Murray's Law.

The input is the Radius of the parent branch.
R1 gets assigned a value in the range of [0.5,0.8]*Radius
R2 is assigned based on Murray's Law.
For now it works only for single branching

-create_2D_vessel: Makes a vessel in 2D which has bifurcations

First it creates a main branch

Then a random walk is taken across the branch
Parameters of sub_branch, such as probability of branching and size of branch are set based on the branch rank of the current point 
Branch rank corresponds to the level of branching the algorithm is currently. Later on, the random walk starts going through branches and sub-branches
At each point, with a probability of p_branch, it determines whether to branch or not

If it branches at this point:
.first it gets the points of this new branch
.Then it calculates the radii of the two daughter branches, as now from the branching point, the vessel goes in two different directions. 
	Note that only one branch is done at a branching point at this time. These are saved in the radius array for later use.
.After this it goes to update the vessel with these new points and the algorithm is assigned to traverse these points later
.Then a certain number of points are skipped in the vessel to avoid branching points that are close to each other.

After the whole vessel has been generated with its branches, an intesnity profile is done. This profile is done by using the distanceFromVessel2D fucntion
As the location of the points is outputed in the array "loc", then assigne_Radius is calculated by using the assignRadiusToPixel code 
as each pixel uses the radius that corresponds to its nearest point to the vessel in the intensity calculation.




-create_branch: Generates centre line points of a branch

1)It takes as input the starting point point and the starting direction, either being generated beforehand by setStartSideAndDir2D,
or the starting point being a branching point and the direction being normal to the branch the bifurcation is occuring at.
2)It initializes points by taking steps that are in the same direction to some extent
That extent is parametrized by the variable "conformity"
3) The initial points are then interpolated using cubic spline
Note that it also outputs the corresponding tangeants of each of the spline points, in Vessel_Dir. 
This will be used if a point appears to be a branching point and we need to know in which direction to take our bifurcation.


-curvature:
used to calculate kappa, but still not used.



-demo_2D_vessel: 
It is the script that runs the create_2D_vessel. Some parameters can be controlled in this script



-distanceFromVessel2D:

Calculates the distance of each pixel from the centre line of the vessel. 
It outputs the distances in the array "distance" and outputs the index of the point from which the distant is calculated in the array 'loc'
It does this by taking each pixel, going over every point along the centre line(defined by the spline beforehand)
and then saves the minimum distance into a matrix


-getCurrentRadius: Gets Radius of current branch.
This is needed as in calculating the daughter branches' radii, the parent's branch needs to be known
Radius_Array is where the radii of the different branches are used.
The current index is taken as input so to compare with the saved indeces in Radius_Array(which are sorted)
So as long as the current index is bigger than what the index value in Radius_Array, then the temp_radius value is updated with the value in Radius_Array
As they are sorted, if the index is smaller than what it is being compared to.

This should be bug free as in principle, a branch has indeces in the range (x1,x2) which are exclusive to it. Meaning that if (x3,x4) belong to another branch, then (x3,x4) is outside of (x1,x2).
So consequently, when the algorithm stops, it should have updated after x1 but stops updating after x2.

-getNormal:

This is used when we are branching, as the branches take a direction more or less normal to the vessel(or that's what I assumed)

-getValidDirection:

This is used mostly to decide where to place the next point in the vessel. Can change the variable 'conformity' depending on how much curving is wanted.


-setStartSideAndDir2D:
Chooses one of the 4 sides of the rectangle/ sqaure space, and starting point for our vessel generation.

====================================================================
To do:

-To figure out how to display both intensity values and the spline points so that they correspond to each other. 
Currently, the intensity profile looks like a rotated and squished version of the spline

-Take the synthetic image and apply (possibly) other functions (e.g. Normalisation, or intensity non-linear response; for THIS, you CAN consult Zehra�s 2D code. But make sure you do the application of non-linearities AFTER step 5.