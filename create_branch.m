function [VesselAxisGT, VesselDir, polyform] = create_branch(NBigSteps, npoints, ...
xRange, yRange, u1, Points, xp, yp, conformity)

%xp and yp are the coordinates of the starting point

extra = 0.0; %this randomizes the length

%The next line is what determines the length of the vessel.
%For the time being, it was done by defining a rectangular space (in x and
%y)where the branch can extend, more or less.

StepSize = sqrt(xRange^2+yRange^2)/(2*NBigSteps); %

for n = 1:NBigSteps
    xp = StepSize*u1(1) + xp + extra*randn*xRange; 
    yp = StepSize*u1(2) + yp + extra*randn*yRange;
    
    % Change direction
    u1 = getValidDirection(u1, conformity); %control vessel curving from here
%    VesselDir(1+n,:) = u1';
    Points=[Points,[xp;yp]];
end    

t = linspace(0,1,size(Points,2));
tt = linspace(0,1,npoints);

polyform = csapi(t, Points);
VesselAxisGT = fnval(polyform,tt);

p1 = fnder(polyform,1);
VesselDir = fnval(p1,tt); %need to normalize this