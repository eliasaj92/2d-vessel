function [VesselAxisGT, Radius_Array, polyform] = create_2D_vessel(npoints, NBigSteps,...
    butterOrder, Radius, Noise )

%This code is supposed to be expanded to the 3D case and take into account
%anisotropic  spatial sampling

%TODO: Have M, N already given as parameters in the function
%and be different from npoints
M = npoints;  
N = npoints;

%if dx=/dy then have anisotropy
%TODO: dx and dy as paramters in function
dx = 1; %micron
dy = 1;

xlocs = dx*(-N/2:N/2);
ylocs = dy*(-M/2:M/2);

xRange = xlocs(end)-xlocs(1);
yRange = ylocs(end)-ylocs(1);

[X,Y] = meshgrid(xlocs,ylocs); %our 2D grid, not used here


[xp,yp, v] =  setStartSideAndDir2D(xlocs,ylocs); %chooses randomly one 
%of the 4 sides of the square surface to start from
Points = [xp;yp];

conformity = 0.9; %TODO: reevaluate value and possibly have more control
%over bending

u1  = getValidDirection(v,conformity );
VesselDir(1,:) = u1';

%make main branch
[VesselAxisGT, VesselDir, polyform] = create_branch(NBigSteps, npoints, ...
xRange, yRange, u1, Points, xp, yp, conformity);


polyform.id = 1;

v_length = size(VesselAxisGT, 2); %main branch length

Radius_Rank = ones(1, v_length); % radius rank is used to determine 
%radius to be used at point
Branch_rank = ones(1,v_length); %branch length is used to determine 
%length of branch at point

%note that Radius rank changes updates at every branching point
%as what is after the branching point will have a different radius to what
%is before the radius point

%Branch rank updates only when we're done going through the main branch,
%then the sub branches, then sub-sub-branches and so on
%this is to let the algorithm know how long the branch should be.
%the branch rank will determine what xRange, yRange and npoints values to
%be used in the create_branch algorithm.
%In other words, Branch rank is the "level"
%It might be that sub_branches branch more. So  the
%porbability of branching also will get updated when the branch 
%rank increases.

Parent = zeros(1,size(VesselAxisGT,2)); 
%reference the parent index represented by the Radius Rank of the
%parent. Names might need to be changed.

VesselAxisGT = [VesselAxisGT; Radius_Rank; Branch_rank];

%division by 3 for now is arbitrary.

npoints1 = floor(npoints/3);
x_r = floor(xRange/2);
y_r = floor(yRange/2);
conformity = sqrt((conformity^2+3)/4); %make sub-branches straighter
temp_br = 1; %we use this value to determine what level of branching
%the algorithm is currently at



%random walk on main branch to determine branching points
%so far, only one branch per point
i = 1;
initial_pBranch = 0.0; %branching probability
Radius_Array = [Radius; 1];
temp_Radius = Radius;%Radius to use for calcuations of daughter vessel radii

incremental_pBranch = 0.005;

pBranch = initial_pBranch;

 while ((i<v_length+1) & (i <200) &(npoints1>=5))
     
     branch = []; %array for our new branch points
     branch_dir = []; %this will be needed to get later branches
     rad_r = [];
     branch_r = [];
     
     if (VesselAxisGT(4,i) > temp_br) %check branch rank
             %if rank is bigger than before, then update branching
             %parameters such as branch length and probability of branching
             temp_br = VesselAxisGT(4,i);
             npoints1 = floor(npoints1*(temp_br-1)/3);
             x_r = floor(x_r/2);
             y_r = floor(x_r/2);
             incremental_pBranch = incremental_pBranch +0.0001;
             pBranch = initial_pBranch;
             conformity = sqrt((conformity^2+1)/2); %make sub-branches straighter
             NBigSteps = NBigSteps -2;
             if NBigSteps <5
                 NBigSteps = 5;
             end
     end
        
     coin_toss = rand(1,1);
     is_branching_point = coin_toss<pBranch; %probability to branch at this point
     
     if is_branching_point == 1
         u = getNormal(VesselDir(:,i)'); %want our new branch to be in another 
         %direction than where we are branching off
         Points = [VesselAxisGT(1,i);VesselAxisGT(2,i)];
         
         
     [branch, branch_dir, temp_poly] = create_branch(NBigSteps, npoints1, ...
x_r, y_r, u, Points, VesselAxisGT(1,i), VesselAxisGT(2,i), conformity); 

     VesselAxisGT(3,(i):end) = VesselAxisGT(3,(i):end)+1;
     rad_r = VesselAxisGT(3,(end))*ones(1,size(branch,2)) +1; %this line and above
     %will help us keep track of the radius to be used when calculating the
     %intensity in the space
     %The Radius will depend on the radius rank of the point
     
     
     %getting new radius values
     temp_Radius = getCurrentRadius(Radius_Array, i);
     [R1, R2] = assignRadius(temp_Radius); % R1>=R2 gets news Radius values
          % daughter vessels where R1>=R2;
     R1 = [R1; i]; %takes index of branching point
     Radius_Array = [Radius_Array, R1];
     R2 = [R2; size(VesselAxisGT,2)+1]; %takes the index of starting point of
     %new branch
     Radius_Array = [Radius_Array, R2];
     
     Radius_Array =sortrows(Radius_Array',2)'; %sorts radius array 
     %according to the indeces where the corresponding branches start
     
     ind_p = VesselAxisGT(3,i) == VesselAxisGT(3,i:end);
     sp = size(ind_p,2);
     ind_p = [zeros(1,i-1), ind_p];
     ind_p = logical(ind_p);
     
     temp_p = VesselAxisGT(3,i-1)*ones(1,size( branch,2));
     Parent(ind_p) = VesselAxisGT(3,i-1);
     Parent = [Parent, temp_p];
     
     %update vessel with new branch
     branch_r = temp_br*ones(1,size(branch,2)) + 1;
     temp_poly.id = size(VesselAxisGT,2)+1; %take index of starting point of new branch
     btemp = [branch; rad_r; branch_r];
     VesselAxisGT = [VesselAxisGT, btemp];
     VesselDir = [VesselDir, branch_dir];
     polyform = [polyform, temp_poly];
     v_length = size(VesselAxisGT,2); %by updating v_length, we can now 
     %include branching in branches!
     %if we want to remove sub-branching, we can just remove that line
     %the rest of the code will work well.
     
     %i = i+5; %skip a few points before deciding to branch again
     pBranch = initial_pBranch - incremental_pBranch; %reset pBranch
     end
     
     pBranch = pBranch +incremental_pBranch; %increase branching probabilty
     i = i+1;
     
 end 
 
 temp_Rank = [];
 
 for j= 1:size(Radius_Array,2)
     temp_Rank = [temp_Rank, VesselAxisGT(3,Radius_Array(2,j))];
 end
 %Radius Array contains the radius and its correspoding rank
 %this is designed so that the radius lookup is based on rank.
 Radius_Array = [Radius_Array; temp_Rank];
 VesselAxisGT = [VesselAxisGT; Parent];
 
hr=plot(VesselAxisGT(1,:),VesselAxisGT(2,:),'r.');
set(hr,'LineWidth',3);
hold on;

%kappa = curvature( polyform, tt); %use this in create_branch code

% xlocs1 = dx*(-N*1.2/2:1.5:N*1.2/2);
% ylocs1 = dy*(-M*1.2/2:2:M*1.2/2);
% %note that the space we're drawing our vessel in slightly bigger to try to
% %contain the whole vessel
% 
% 
% [distance, loc] = distanceFromVessel2D(VesselAxisGT, xlocs1, ylocs1);
% 
% assigned_Radius = assignRadiusToPixel(loc, Radius_Array);
% 
% intensity = 1./(1+(distance./assigned_Radius).^butterOrder);
% 
% R = intensity + Noise*randn(size(intensity));

end