function limits = vesselLimits(Vessel, Radius_Array, ...
    ButterOrder, Intensity_Threshold)

%this function is for optimizing the calculation of the intensity
%Basically, we find the limits of the square surrounding the vessel where
%in only intensity values higher than a certain threshold realtive to the
%maximum itensity is contained.

n = (1/Intensity_Threshold)^(1/ButterOrder) - 1;

m = size(Vessel, 2);

xmin = inf;
ymin = inf;
xmax = -inf;
ymax = -inf;


for i = 1:m

    R = Radius_Array(1, Radius_Array(3,:) == Vessel(3,i) );
    
    xmin_temp = Vessel(1,i) - n*R;
    xmax_temp = Vessel(1,i) + n*R;
    ymin_temp = Vessel(2,i) - n*R;
    ymax_temp = Vessel(2,i) + n*R;
    
    if xmin_temp <xmin
        xmin = xmin_temp;
    end
    
    if xmax_temp>xmax
        xmax = xmax_temp;
    end
    
    if ymin_temp<ymin
        ymin = ymin_temp;
    end
    
    
    if ymax_temp>ymax
        ymax = ymax_temp;
    end
     

end

limits = [xmin xmax; ymin ymax];

end