function clash =  checkClash(x1,y1,x2,y2,x3,y3,x4,y4)
%checks if two segments intersect
%works in 2D for the time being

E = [x1-x2,y1-y2];
F = [x3-x4,y3-y4];
G = [x2-x4,y2-y4];
P = [-E(1,2), E(1,1)];

temp = G*P'./(F*P');



clash = temp>0 & temp<1;



end