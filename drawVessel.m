function R = drawVessel(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy )
%Vessel is the described centreline in the xy space as is.
%This script is supposed to give us the drawing of the vessel
%when accounting for anisotropy
%nx smapling points in x; dx resolution in x
%Similary for ny and dy

xlocs = dx*(-nx/2:nx/2);
ylocs = dy*(-ny/2:ny/2);
%note that the space we're drawing our vessel can can be bigger than the
%vessel, containing it, or smaller depending on the range of xlocs/ylocs

R = zeros(nx+1,ny+1); % zeros(size(xlocs,2),size(ylocs,2));

%limits = vesselLimits(Vessel,Radius_Array,butterOrder,0.05);

%a simple way to compare this curent optimized form with the unoptimized
%one is to take limits = [-inf inf; -inf inf]

%[distance, loc] = distanceFromVessel2D(Vessel, xlocs, ylocs, limits);
[distance, loc] = distanceFromVessel2DMesh(Vessel, xlocs, ylocs);

assigned_Radius = assignRadiusToPixel(loc, Radius_Array);

intensity = 1./(1+(distance./assigned_Radius).^butterOrder);

R = intensity + Noise*randn(size(intensity));

figure; imagesc(xlocs,ylocs,R);
end 