function temp_Radius = getCurrentRadius(Radius_Array, index)
%this is another version of assignRadiusToPixel
%However, assignRadiusToPixel can handle index being an array or matrix.
%this hasn't been tested for this purpose


m = size(Radius_Array,2);

j = 1;

while (j<m+1 & index>=(Radius_Array(2,j)) )
    temp_Radius = Radius_Array(1,j);
    j=j+1;
end

end