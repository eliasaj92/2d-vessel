function [VesselSkeleton, Radius_Array, polyform] = create_2D_vessel2(npoints, NBigSteps,...
    butterOrder, segment_size, Radius, Noise )
%This one is supposed to work using Vessel segments. Beforehand, a whole
%vessel was contructed


%This code is supposed to be expanded to the 3D case and take into account
%anisotropic  spatial sampling

%TODO: Have M, N already given as parameters in the function
%and be different from npoints
M = npoints;  
N = npoints;

%if dx=/dy then have anisotropy
%TODO: dx and dy as paramters in function
dx = 1; %micron
dy = 1;

xlocs = dx*(-N/2:N/2);
ylocs = dy*(-M/2:M/2);

xRange = xlocs(end)-xlocs(1);
yRange = ylocs(end)-ylocs(1);

[X,Y] = meshgrid(xlocs,ylocs); %our 2D grid, not used here


[xp,yp, v] =  setStartSideAndDir2D(xlocs,ylocs); %chooses randomly one 
%of the 4 sides of the square surface to start from
Points = [xp;yp];

conformity = 0.9; %TODO: reevaluate value and possibly have more control
%over bending

u1  = getValidDirection(v,conformity );
VesselDir(1,:) = u1';

%Generate first segment
[VesselSkeleton, VesselDir, polyform] = create_branch_segment...
    ([], NBigSteps, npoints, segment_size, u1, Points, conformity);

polyform.id = 1;

v_length = size(VesselSkeleton, 2); %main branch length

Radius_Rank = ones(1, v_length); % radius rank is used to determine 
%radius to be used at point
Branch_rank = ones(1,v_length); %branch length is used to determine 
%length of branch at point

%note that Radius rank changes updates at every branching point
%as what is after the branching point will have a different radius to what
%is before the radius point

%Branch rank updates only when we're done going through the main branch,
%then the sub branches, then sub-sub-branches and so on
%this is to let the algorithm know how long the branch should be.
%the branch rank will determine what xRange, yRange and npoints values to
%be used in the create_branch algorithm.
%In other words, Branch rank is the "level"
%It might be that sub_branches branch more. So  the
%porbability of branching also will get updated when the branch 
%rank increases.

Parent = zeros(1,size(VesselSkeleton,2)); 
%reference the parent index represented by the Radius Rank of the
%parent. Names might need to be changed.

VesselSkeleton = [VesselSkeleton; Radius_Rank; Branch_rank];

%division by 3 for now is arbitrary.

npoints1 = floor(npoints/3);
x_r = floor(xRange/2);
y_r = floor(yRange/2);
conformity = sqrt((conformity^2+3)/4); %make sub-branches straighter
temp_br = 1; %we use this value to determine what level of branching
%the algorithm is currently at



%random walk on main branch to determine branching points
%so far, only one branch per point
i = 1;
Radius_Array = [Radius; 1];
temp_Radius = Radius;%Radius to use for calcuations of daughter vessel radii


theta1 = 10*pi/180; %angle of bigger bifurcation
theta2 = 40*pi/180; %of smaller one

u2 = [];

 while ((i<v_length+1) & (i <300) &(npoints1>=5))
     
     branch1 = []; %array for our first new branch points
     branch_dir1 = []; %this will be needed to get later branches
     rad_r1 = [];
     branch_r1 = [];
     
     branch2 = []; %array for our second new  branch points
     branch_dir2 = [];
     rad_r2 = [];
     branch_r2 = [];
     
             temp_br = VesselSkeleton(4,i);
             t_npoints = floor(npoints/1+temp_br);
             t_size = segment_size/temp_br;
            % conformity = sqrt((conformity^2+(1+temp_br))/(2+temp_br)); %make sub-branches straighter
             NBigSteps = NBigSteps -2;
             if NBigSteps <5
                 NBigSteps = 5;
             end
     
   
     
      if (i == v_length) | (VesselSkeleton(3,i) ~= VesselSkeleton(3,i+1))
          
          %temp_Radius = getCurrentRadius(Radius_Array, i); 
          temp_Radius = assignRadiusToPixel(i,Radius_Array);
          %TODO: somehow be able to get distance/ size of segment ratio from
          %fucntion below
          [R1, R2] = assignRadius(temp_Radius); % R1>=R2 gets news Radius values
          % daughter vessels where R1>=R2;
              
          
          
          [u1, u2] = getBifurcationDirections(VesselDir(:,i)',theta1,...
              theta2); %want our new branch to be in another 
         %direction than where we are branching off
         Points = [VesselSkeleton(1,i);VesselSkeleton(2,i)];
         
         
         
            [branch1, branch_dir1, temp_poly1] =  create_branch_segment...
    (VesselSkeleton,NBigSteps, npoints, segment_size, u1, Points, conformity) ;
     if (size(branch1) ~= size([]))
     %VesselSkeleton(3,(i):end) = VesselSkeleton(3,(i):end)+1;
     
     rad_r1 = VesselSkeleton(3,(end))*ones(1,size(branch1,2)) +1; %this line and above
     %will help us keep track of the radius to be used when calculating the
     %intensity in the space
     %The Radius will depend on the radius rank of the point
    
     
     
     temp_p = VesselSkeleton(3,i-1)*ones(1,size( branch1,2));
     Parent = [Parent, temp_p];
     
     %update vessel with new branch
     branch_r1 = temp_br*ones(1,size(branch1,2)) + 1;
     temp_poly1.id = size(VesselSkeleton,2)+1; %take index of starting point of new branch
     btemp = [branch1; rad_r1; branch_r1];
     VesselSkeleton = [VesselSkeleton, btemp];
     VesselDir = [VesselDir, branch_dir1];
     if (temp_poly1.addToArray == 1 )
        polyform = [polyform, temp_poly1];
     end
     
     R1 = [R1; v_length+1]; %takes index of branching point
     Radius_Array = [Radius_Array, R1];
     
     v_length = size(VesselSkeleton,2); %by updating v_length, we can now 
     %include branching in branches!
     %if we want to remove sub-branching, we can just remove that line
     %the rest of the code will work well.

%    [R1, R2] = assignRadius(temp_Radius); % R1>=R2 gets news Radius values
%           % daughter vessels where R1>=R2;
%      R1 = [R1; v_length]; %takes index of branching point
%      Radius_Array = [Radius_Array, R1];
     
     %first branch created now onto the second
     
     end
     
    [branch2, branch_dir2, temp_poly2] =  create_branch_segment...
    (VesselSkeleton,NBigSteps, npoints, segment_size/1.3, u2, Points, conformity) ;
    
    if (size(branch2) ~= size([]))
    %VesselSkeleton(3,(i):end) = VesselSkeleton(3,(i):end)+1;
     
     rad_r2 = VesselSkeleton(3,(end))*ones(1,size(branch2,2)) +1; %this line and above
     %will help us keep track of the radius to be used when calculating the
     %intensity in the space
     %The Radius will depend on the radius rank of the point
    
     
     
     temp_p = VesselSkeleton(3,i-1)*ones(1,size( branch2,2));
     Parent = [Parent, temp_p];
     
     %update vessel with new branch
     branch_r2 = temp_br*ones(1,size(branch2,2)) + 2;
     temp_poly2.id = size(VesselSkeleton,2); %take index of starting point of new branch
     if (temp_poly2.addToArray == 1 )
        polyform = [polyform, temp_poly2];
     end
     btemp = [branch2; rad_r2; branch_r2];
     VesselSkeleton = [VesselSkeleton, btemp];
     VesselDir = [VesselDir, branch_dir2];
     
     R2 = [R2; v_length+1]; %takes the index of starting point of
     %new branch
     Radius_Array = [Radius_Array, R2];
     
     Radius_Array =sortrows(Radius_Array',2)'; %sorts radius array 
     %according to the indeces where the corresponding branches start
       
     
     v_length = size(VesselSkeleton,2); %by updating v_length, we can now 
     %include branching in branches!
     %if we want to remove sub-branching, we can just remove that line
     %the rest of the code will work well. 
     
    end
     
        
      end 
     
     i = i+1;
     
 end 
 
 temp_Rank = [];
 
 for j= 1:size(Radius_Array,2)
     temp_Rank = [temp_Rank, VesselSkeleton(3,Radius_Array(2,j))];
 end
 %Radius Array contains the radius and its correspoding rank
 %this is designed so that the radius lookup is based on rank.
 Radius_Array = [Radius_Array; temp_Rank];
 VesselSkeleton = [VesselSkeleton; Parent];
  
 
hr=plot(VesselSkeleton(1,:),VesselSkeleton(2,:),'r.');
set(hr,'LineWidth',3);
hold on;

%kappa = curvature( polyform, tt); %use this in create_branch code

% xlocs1 = dx*(-N*1.2/2:1.5:N*1.2/2);
% ylocs1 = dy*(-M*1.2/2:2:M*1.2/2);
% %note that the space we're drawing our vessel in slightly bigger to try to
% %contain the whole vessel
% 
% 
% [distance, loc] = distanceFromVessel2D(VesselSkeleton, xlocs1, ylocs1);
% 
% assigned_Radius = assignRadiusToPixel(loc, Radius_Array);
% 
% intensity = 1./(1+(distance./assigned_Radius).^butterOrder);
% 
% R = intensity + Noise*randn(size(intensity));

end