function [xp,yp, v] = setStartSideAndDir2D(xlocs,ylocs)

faceindex = randi(4,1);
switch(faceindex)
    case 1 
        yp = ylocs(1);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        v = [0 1];
    case 2
        xp = xlocs(1);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        v = [1 0];
    case 3
        yp = ylocs(end);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        v = [0 -1];
    case 4
        xp = xlocs(end);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        v = [-1 0];
end
