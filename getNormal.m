function u=getNormal(v)

%gets a normal direction

v = v/norm(v); %normalize v.
conformity = 0.8; %decrease to make it more normal

NoValidDirection = true;
while NoValidDirection 
    u = makerandunitdirvec(1);
    dp = v*u';
    if (dp<conformity) & (dp>0.3 )
        NoValidDirection = false;
    end
end
end

%the following works for 2D only

function u1=makerandunitdirvec(N)
v = randn(N,2);
u1 = bsxfun(@rdivide,v,sqrt(sum(v.^2,2)));
end