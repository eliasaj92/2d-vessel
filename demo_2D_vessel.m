NBigSteps = 8;
npoints = 100; %npoints = nx = ny for the time being
butterOrder = 6;
Radius = 4;
Noise = 0;
nx = 100; %nx and ny are the sampling
ny = 100;
dx = 2; %micron
dy = 2;


close all;

[ Vessel, Radius_Array, polyform] = create_2D_vessel(npoints,NBigSteps,butterOrder,Radius,Noise);

R = drawVessel(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy );
R1 = drawVessel(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy );