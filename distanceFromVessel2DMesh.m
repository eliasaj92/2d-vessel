function [distance, loc] = distanceFromVessel2DMesh(Vessel, x, y)

%This code calculates the distance of each pixel from the vessel
%This uses mesh grids for calculations of the minimum

[X,Y] = meshgrid(x,y);

c = size(Vessel,2);


distance = inf*ones(size(X));
loc = ones(size(X)); %stores index of point on vessel

for i = 1:c
    mx = Vessel(1,i);
    my = Vessel(2,i);
    tdist = sqrt((X-mx).*(X-mx) + (Y-my).*(Y-my));
    ind = tdist < distance;
    distance(ind) = tdist(ind);
    loc(ind) = i;
end


end