NBigSteps = 8;
npoints = 20; %npoints = nx = ny for the time being
butterOrder = 6;
Radius = 4;
Noise = 0;
nx = 100; %nx and ny are the sampling
ny = 100;
dx = 2; %micron
dy = 2;
segment_size = 50;

close all;

[ Vessel, Radius_Array, polyform] = create_2D_vessel2(npoints, NBigSteps,...
    butterOrder, segment_size, Radius, Noise );

R1 = drawVesselMesh(Vessel, Radius_Array,butterOrder, Noise, nx, ny, dx, dy );