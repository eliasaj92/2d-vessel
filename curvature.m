function kappa = curvature( polyform, t)

%This calculates the curvature of the cubic spline
%It returns the curvature at every element of t
%note that the equations were taken from the following link
%http://mathwiki.ucdavis.edu/Calculus/Vector_Calculus/Vector-Valued_Functions_and_Motion_in_Space/Curvature_and_Normal_Vectors_of_a_Curve

%here polyform is the equivalent of r(t) in the above link
%Even though the above link gives the case for 3D, it's easy to translate
%it to 2D since the z dimension can be set to zero

%This code has been tested for 2D but should work in 3D

%Please note that it is not perfect. I tested it on a straight line as a
%sanity check and got values of magnitude 10^-2
%I treated those values as small enough, due the the deviations made
%in the cubic spline interpolation function csapi



n = size(t,2);

pp1 = fnder(polyform,1);
R1 = fnval(pp1,t);% r'(t)
pp2 = fnder(pp1,1);
R2 = fnval(pp2,t);% r''(t)


if polyform.dim == 2 %if working in 2D
    R1 = [R1; zeros(1,n)];
    R2 = [R2; zeros(1,n)];
end 
%From the link, I used the last equation in the section:
%"Definition of Curvature (repeat)"


num = zeros(1,n);
den = zeros(1,n);
for i=1:n
    
    temp = cross(R1(:,i),R2(:,i)); 
    
    num(1,i) = norm(temp );  
    
    temp1 = norm(R1);
    
    den(1,i) = temp1^3;
end

kappa = num./den;

end