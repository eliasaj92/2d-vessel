function [Vessel] = rebuildVessel(VesselStructure)

m = size(VesselStructure,2);
Vessel = [];
for i = 1:m
    
    tt = linspace(0,1,VesselStructure(i).npoints);
    V = fnval(VesselStructure(i),tt);
    Radius_Rank = VesselStructure(i).id*ones(1,size(V,2));
    Branch_Rank = VesselStructure(i).Rank*ones(1,size(V,2));
    parent = VesselStructure(i).parent_id*ones(1,size(V,2));
    V = [V;Radius_Rank;Branch_Rank;parent];
    Vessel = [Vessel, V];
    
end


end