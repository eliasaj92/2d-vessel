function [distance, loc] = distanceFromVessel2D(Vessel, x, y,limits)

%This code calculates the distance of each pixel from the vessel
%Note that x and y are the pixel locations coordinates

%Implemented this without vectorization
%Not sure how to go about doing that for the time being

n = size(x, 2);
m = size(y,2);

c = size(Vessel,2);


distance = inf*ones(n,m);
loc = ones(n,m); %stores index of point on vessel

ramp_x = 1:n;
ramp_y = 1:m;

x_index = x>limits(1,1)&x<limits(1,2);
y_index = y>limits(2,1)&y<limits(2,2);

for i =ramp_x(x_index) %1:n
    for j =ramp_y(y_index) %1:
        %if (x(1,i)>=limits(1,1)) & (x(1,i)<=limits(1,2)) %check if x in range
        %    if (y(1,i)>=limits(2,1)) & (y(1,i)<=limits(2,2)) %check if y in range
                temp = [x(1,i); y(1,j)]; %pixel location
                d = inf;
                for k = 1:c
                    dTemp = norm(Vessel(1:2,k) - temp);
                    if dTemp < d
                        d = dTemp;
                        loc(i,j) = k;
                    end
                end
                distance(i,j) = d;
        
         %   end
        %end
    end

end


end